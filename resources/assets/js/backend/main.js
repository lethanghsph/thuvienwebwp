(function( document, $ ){
    'use strict';

    window.Webviet = window.Webviet || {};

    var Extra = {
    	initExtra: function()
    	{
            this.ingSelect();
            this.chosenSelect();
            this.dataTable();
    		this.tooltip();
    		this.metisMenu();
    		this.resizeSidebar();
    	},
        ingSelect: function()
        {
            $('#lfm').filemanager('image');
        },
        chosenSelect: function()
        {
            $('.wv-chosen-select').chosen();
        },
    	dataTable: function()
    	{
    		$('#dataTables-example').DataTable({
	            responsive: true,
	            order: [[ 0, "desc" ]]
	        });
    	},
    	tooltip: function()
    	{
    		$('[data-tooltip="tooltip"]').tooltip();
    	},
    	metisMenu: function()
    	{
    		$('#side-menu').metisMenu();
    	},
    	// Clone SB Admin2.
    	resizeSidebar: function()
    	{
    		$(window).bind("load resize", function() {
		        var topOffset = 50;
		        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		        if (width < 768) {
		            $('div.navbar-collapse').addClass('collapse');
		            topOffset = 100; // 2-row-menu
		        } else {
		            $('div.navbar-collapse').removeClass('collapse');
		        }

		        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
		        height = height - topOffset;
		        if (height < 1) height = 1;
		        if (height > topOffset) {
		            $("#page-wrapper").css("min-height", (height) + "px");
		        }
		    });

		    var url = window.location;
		    var element = $('ul.nav a').filter(function() {
		        return this.href == url;
		    }).addClass('active').parent();

		    while (true) {
		        if (element.is('li')) {
		            element = element.parent().addClass('in').parent();
		        } else {
		            break;
		        }
		    }
    	},
    }
    $(document).ready(function() {
        // Extend Webviet method.
        $.extend(true, Webviet, Extra);

        // Run.
        Webviet.initExtra();
    });
})( document, jQuery );
