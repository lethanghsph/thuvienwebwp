require('./menu');


(function( document, $ ){
	'use strict';

	window.Starter = window.Starter || {};

	var Extra = {
		initExtra: function() {
			this.handleVideoPostFormat();
			this.desktopMenuAffix();
			this.mobileMenuAffix();
			this.menu();
			this.getSlick();
			this.tooltip();
		},
		// Set post format video
		handleVideoPostFormat: function() {
			$(document).on('click', '.entry-media-oembed > a', function(e) {
				e.preventDefault();
				$(this).closest('.entry-media-oembed').addClass('open');
			});
		},
		desktopMenuAffix: function() {
			var $mobileAffix = $('.wv-header-desktop.wv-header-affix'),
				$wvOffset = $('.wv-header-desktop').height() * 2;

			/* Set offset */
			$( $mobileAffix ).affix({
				offset: $wvOffset
			});
		},
		mobileMenuAffix: function() {
			var $mobileAffix = $('.wv-header-mobile.wv-header-affix'),
				$wvOffset = $('.wv-header-mobile').height();

			/* Set offset */
			$( $mobileAffix ).affix({
				offset: $wvOffset
			});
		},
		menu: function() {
			$( '.wv-header-mobile' ).wv_menu_mobile();
		},

		// Slick slider
		getSlick: function() {
			$('[data-init="slick"]').each(function (){
				var el = $(this);

				var breakpointsWidth = {tn: 319, xs: 479, ss: 519, sm: 767, md: 991, lg: 1199};

				var slickDefault = {
					// fade: true,
					infinite: true,
					autoplay: true,
					pauseOnHover: true,
					speed: 1000,
					adaptiveHeight: true,

					slidesToShow: 1,
					slidesToScroll: 1,

					mobileFirst: true
				};

				// Merge settings.
				var settings = $.extend(slickDefault, el.data());
				delete settings.init;

				// Build breakpoints.
				if (settings.breakpoints) {
					var _responsive = [];
					var _breakpoints = settings.breakpoints;

					var buildBreakpoints = function (key, show, scroll) {
						_responsive.push({
							breakpoint: breakpointsWidth[key],
							settings: {
								slidesToShow: parseInt(show),
								slidesToScroll: 1
							}
						});
					};

					if (typeof _breakpoints === "object") {
						$.each(_breakpoints, buildBreakpoints);
					}

					delete settings.breakpoints;
					settings.responsive = _responsive;
				};

				el.slick(settings);
			 });
		},

		tooltip: function() {
			 $('[data-tooltip="tooltip"]').tooltip();
		},

	};

	$(document).ready(function(){
		// Extend Starter method.
		$.extend(true, Starter, Extra);

		// Run.
		Starter.initExtra();
	});	
})( document, jQuery );


