/**
 *
 * -----------------------------------------------------------
 *
 * Starter mobile menu js
 *
 * -----------------------------------------------------------
 *
 */

(function( document, $, undefined ){
	'use strict';

	// Step 2: Method of plugin.
	var methods = (function() {
		var menu = {};

		// Vertical menu.
		(function(){
			function mobile( $menu ) {
				var _this = this,
					mobile = {
						icon: $menu.find('.wv-menu-icon'),
						navs_slide: $menu.find('.wv-header-side'),
						overlay: $menu.find('.wv-body-overlay'),
						subicon: $menu.find( '.submenu-icon' ),
					};

				// Run if header side is exists.
				_this.ToggleSide( mobile );
				mobile.subicon.each(function(index, el) {
					$(el).on('click', function(event) {
						var $el = $(this);
						_this.ToggleSubmenu( $el );
					});
				});
			}

			mobile.prototype.Toggle = function( mobile ) {
				var _this = this,
					width = mobile.navs_slide.width();

				if ( ! mobile.icon.hasClass('open') ) {
					mobile.icon.addClass('open');
					mobile.overlay.fadeIn(300);
					mobile.navs_slide.animate({ right: 0 }, 300);
					$('body').animate({ left: -width }, 300);
				} else {
					mobile.icon.removeClass('open');
					mobile.overlay.fadeOut(300);
					mobile.navs_slide.animate({ right: -width }, 300);
					$('body').animate({ left: 0 }, 300);
				}
			}
			mobile.prototype.ToggleSide = function( mobile ) {
				var _this = this;
				mobile.icon.on( 'click', function(e) {
					e.preventDefault();
					_this.Toggle( mobile );
					var icon = e.target;
					mobile.icon.find('i').removeClass('fa-bars').addClass('fa-times');
				});
				mobile.overlay.on( 'click', function() {
					_this.Toggle( mobile );
					mobile.icon.find('i').removeClass('fa-times').addClass('fa-bars');
				});
			}
			mobile.prototype.ToggleSubmenu = function( $el ) {
				event.preventDefault();

				var $el_next = $el.closest('li').siblings().find('i'),
					ul = $el.closest('li').find('ul:first'),
					ul_next = $el.closest('li').siblings().find('ul');
					
				// Toggle.
				$el_next.stop(true, true).removeClass('active');
				$el.find('i').toggleClass('active');
				ul_next.stop(true, true).slideUp('1000');
				ul.stop(true, true).slideToggle('1000');
			}
			menu.mobile = mobile;
		})();

		// Publish method.
		return {
			// destroy: function() {

			// },
			init: function(options) {
				return this.each(function () {
					var $this = $(this);
					if ($this.data('wv_menu_mobile')) {
						return;
					}
						
					$this.data('wv_menu_mobile', options);

					new menu.mobile( $this);

				});
			}
		};
	
	})();
	// Step 3: Initialize plugin.
	$.fn.wv_menu_mobile = function (method, args) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		}
		else {
			return $.error('Method ' +  method + ' does not exist on jQuery.fn.wv_menu_mobile');
		}
	};
})( document, jQuery );

