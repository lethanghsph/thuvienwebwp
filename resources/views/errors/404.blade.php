@extends('webviet::frontend.layout.main')
@section('title')
	404
@endsection
@section('page-title')
	<p style="font-size: 72px" class="mb-40">404</p><br>
	<span>The resource requested <br> could not be found<br> on this server</span>
@endsection
