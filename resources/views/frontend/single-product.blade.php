@extends('webviet::frontend.layout.main')
@section('title')
	{{$product['title']}}
@endsection
@section('page-title')
	{{$product['title']}}
@endsection

@section('content')
	<div id="primary" class="content-area mt-15 mb-30">
		<div class="container">
			<main id="main" class="site-main" role="main">
				<div class="wv-wrap--prod-sing">
					@include('webviet::frontend.product.single', ['product' => $product])
				</div>
			</main>
		</div>
	</div>
@endsection