<footer id="wv-site-footer" class="wv-site-footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-5 col-sm-push-7">
				<?php $socials = [
					'facebook' => '#',
					'twitter' => '#',
				] ?>
				@if( isset($socials) )
					<ul class="wv-social wv-social--profile wv-social--footer">
						@foreach( $socials as $key => $social )
							<li><a href="<?php print( $social ); // WPCS: XSS OK. ?>" target="__blank"><i class="fa fa-<?php print $key; // WPCS: XSS OK. ?>"></i></a></li>
						@endforeach
					</ul>
				@endif
			</div>
			<div class="col-xs-12 col-sm-7 col-sm-pull-5">
				<p class="wv-copyright">Copyright &#169;webviet.com</p>
			</div>
		</div>
	</div>
</footer><!-- #wv-site-footer -->