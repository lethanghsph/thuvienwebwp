<div class="wv-header-menu">
	<div class="clearfix">
		<nav role="navigation" aria-label="Primary Menu Desktop">
			<ul class="navs navs-primary navs--horizontal">
				@include('webviet::frontend.navigation.partials.primary')
			</ul>
		</nav><!-- #site-navigation -->
	</div>
</div>
