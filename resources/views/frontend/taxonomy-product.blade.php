@extends('webviet::frontend.layout.main')
@section('title')
	{{$taxonomy}}
@endsection
@section('page-title')
	{{$taxonomy}}
@endsection

@section('content')
	<div id="primary" class="content-area mt-15">
		<div class="container">
			<main id="main" class="site-main" role="main">
				<div class="row">
					<div class="wv-wrap--prod-arc">
					@foreach($products as $product)
						<div class="col-xs-6 col-sm-3 wv-item">
							@include('webviet::frontend.product.archive', ['product' => $product])
						</div>
					@endforeach
						<div class="col-xs-12 wv-pagination text-right">
							{{ $products->links() }}
						</div>
					</div>
				</div>
			</main>
		</div>
	</div>
@endsection