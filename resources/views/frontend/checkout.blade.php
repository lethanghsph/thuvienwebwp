@extends('webviet::frontend.layout.main')
@section('title')
	Checkout
@endsection

@section('page-title')
	Checkout
@endsection

@section('content')
	<div id="primary" class="content-area mt-15 mb-50">
		<div class="container">
			<main id="main" class="site-main" role="main">
				@include('webviet::frontend.checkout.form')
			</main>
		</div>
	</div>
@endsection
