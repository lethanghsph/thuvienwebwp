@section('header-image')
<div class="wv-header-img parallax-window-wrap">
	<div class="parallax-window" data-speed="0.6" data-position-x="right" data-parallax="scroll" data-image-src=""></div>
	<div class="container">
		<h2 class="wv-page-title">
			@section('page-title')
				Home
			@show
		</h2>
	</div>
</div>
@show