<div class="wv-header-desktop wv-header-affix">
	<div class="container wrap">
		@include('webviet::frontend.shared.logo')
		@include('webviet::frontend.navigation.primary_desktop')
	</div>
</div><!-- End header desktop -->
