<?php
/**
 * Displays header mobile
 *
 * @package Starter
 */

?>
<div class="wv-header-mobile wv-header-affix">
	<div class="container">
		<div class="row">
			<div class="col-xs-9">
				@include('webviet::frontend.shared.logo')
			</div>
			<div class="col-xs-3">
				<div class="wv-menu-icon pull-right">
					<i class="fa fa-bars"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="wv-header-side">
		<div class="add-pd clearfix">
			<div class="row">
				<ul class="nav nav-tabs wv-tablist" role="tablist">
					<li role="presentation" class="col-xs-3 item active">
						<a href="#menu" aria-controls="menu" role="tab" data-toggle="tab"><i class="fa fa-bars" aria-hidden="true"></i></a>
					</li>
					<li role="presentation" class="col-xs-3 item">
						<a href="#search" aria-controls="search" role="tab" data-toggle="tab"><i class="fa fa-search"  aria-hidden="true"></i></a>
					</li>
					<li role="presentation" class="col-xs-3 item">
						<a href="#cart" aria-controls="cart" role="tab" data-toggle="tab"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
					</li>
					<li role="presentation" class="col-xs-3 item">
						<a href="#lang" aria-controls="lang" role="tab" data-toggle="tab"><i class="fa fa-language" aria-hidden="true"></i></a>
					</li>
				</ul>
			</div>
			<div class="row">
				
				<div class="tab-content wv-tab-content">
					<div role="tabpanel" class="tab-pane active" id="menu">
						@include('webviet::frontend.navigation.primary_mobile')
					</div>
					<div role="tabpanel" class="tab-pane" id="search">
						<ul class="sub-menu">
							<input type="text" name="search">
						</ul>
					</div>
					<div role="tabpanel" class="tab-pane" id="cart">
						cart
					</div>
					<div role="tabpanel" class="tab-pane" id="lang">
						<ul class="wv-list-none">
							<li><a href="#">VN</a></li>
							<li><a href="#">EN</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="wv-body-overlay"></div>
</div><!-- End header mobile -->


