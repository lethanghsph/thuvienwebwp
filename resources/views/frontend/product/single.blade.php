<article id="wv-prod-{{$product['id']}}" class="wv-article wv-prod-sing">
	<div class="row">
		<div class="col-xs-12 col-sm-5">
			<div class="entry-thumbnail wv-img-zoom">
				<a href="{!! $product['demo'] !!}" target="_blank">
				<img src="{!! url($product['thumbnail']) !!}" alt="{{$product['title']}}">
				</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-7">
			<header class="entry-header">
				<h2 class="entry-title entry-title--haslink wv-prod-sing__title">{{$product['title']}}</h2>
				<p class="wv-prod-sing__sku">
					<span class="bold">SKU:</span> {{$product['sku']}}
				</p>
				@if(!empty($product['tags']))
				<p class="wv-prod-sing__tags">
					<span class="bold">Tags:</span> {!! $product['tags'] !!}
				</p>
				@endif
			</header>
			<div class="entry-content text-justify">
				{!! $product['content'] !!}
			</div>
			<footer class="entry-footer">
				<div class="wv-prod-sing__act text-right">
					<a href="{!! route('webviet.frontend.cart.add', $product['id']) !!}" class="btn btn-warning" rel="nofollow"><i class="fa fa-cart-plus">&nbsp;Buy now</i></a>
					<a href="{!! $product['demo'] !!}" class="btn btn-primary" target="_blank" rel="nofollow"><i class="fa fa-link">&nbsp;Demo</i></a>
				</div>
			</footer>
		</div>
	</div>
</article>
