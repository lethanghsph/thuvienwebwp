<article id="wv-prod-{{$product['id']}}" class="wv-article wv-prod-arc">
	<header class="entry-container">
		<div class="entry-thumbnail wv-img-zoom">
			<img src="{!! url($product['thumbnail']) !!}" alt="{{$product['title']}}">
		</div>
		<div class="wv-prod-arc__act">
			<a href="{!! route('webviet.frontend.cart.add', $product['id']) !!}" class="btn btn-primary" data-tooltip="tooltip" title="Buy now"><i class="fa fa-cart-plus" rel="nofollow"></i></a>
			<a href="{{ route('webviet.frontend.product.single', $product['id']) }}" class="btn btn-primary" data-tooltip="tooltip" title="Detail" rel="nofollow"><i class="fa fa-link"></i></a>
		</div>
	</header>
	<div class="entry-footer">
		<h2 class="entry-title entry-title--haslink wv-prod-arc__title"><a href="{!! url('product/' . $product['id']) !!}">{{$product['title']}}</a></h2>
	</div>
</article>
