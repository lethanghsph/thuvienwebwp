<div class="row">
	<div class="col-xs-12 col-sm-5">
		<div class="entry-thumbnail wv-img-zoom">
			<img src="{!! $product['thumbnail'] !!}" alt="{{$product['title']}}">
		</div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="wv-checkout-order mt-20">
			<h3>Your order</h3>
			<ul class="navs">
				<li><span class="bold">Product: </span><span>{{$product['title']}}</span></li>
				<li><span class="bold">SKU: </span><span>{{$product['sku']}}</span></li>
			</ul>
		</div>
		<div class="wv-checkout-billing mt-20">
			<h3>Billing details</h3>
			@include('webviet::frontend.shared.errors')
			<form action="{!! route('webviet.frontend.checkout.store') !!}" method="POST">
				{{csrf_field()}}
				<input type="hidden" name="product_id" value="{{$product['id']}}">
				<div class="form-group">
					<input type="text" name="name" class="form-control" placeholder="* Your name" value="{!! old('name') !!}" required>
				</div>
				<div class="form-group">
					<input type="text" name="phone" class="form-control" placeholder="* Phone number" value="{!! old('phone') !!}" required>
				</div>
				<div class="form-group">
					<input type="text" name="email" class="form-control" placeholder="* Email" value="{!! old('email') !!}" required>
				</div>
				<div class="form-group">
					<input type="text" name="address" class="form-control" placeholder="Address" value="{!! old('address') !!}">
				</div>
				<div class="form-group">
					<textarea name="note" class="form-control" placeholder="Note" rows="4">{!! old('note') !!}</textarea>
				</div>
				<input type="submit" value="Order" class="btn btn-primary pull-right">
			</form>
		</div>
	</div>
</div>
