<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('webviet::frontend.shared.meta')
        @include('webviet::frontend.shared.css')
    </head>
    <body>
		@include('webviet::frontend.header.header')
		@yield('content')
		@include('webviet::frontend.footer.footer')
		@include('webviet::frontend.shared.js')
   		@yield('unique-js')
    </body>
</html>
