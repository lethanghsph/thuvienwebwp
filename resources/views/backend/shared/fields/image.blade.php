<img id="holder" style="margin-bottom: 15px; max-height:300px;" src="{{$thumbnail}}">
<div class="input-group">
	<span class="input-group-btn">
		<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
			<i class="fa fa-picture-o"></i> Choose
		</a>
	</span>
	<input id="thumbnail" type="text" class="form-control" name="thumbnail" value="{{$thumbnail}}">
</div>