<div class="navbar-default sidebar wv-sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="{{route('webviet.admin.media.index')}}"><i class="fa fa-file fa-fw"></i> Media</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-book fa-fw"></i> Post<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('webviet.admin.post.index') }}">All posts</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.post.create') }}">New post</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.post_tag.index') }}">Tags</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-tag fa-fw"></i> Product<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('webviet.admin.product.index') }}">All products</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.product.create') }}">New products</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.product_tag.index') }}">Tags</a>
                    </li> 
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-tag fa-fw"></i> Order<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('webviet.admin.order.index') }}">All orders</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.order.create') }}">New order</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-book fa-fw"></i>User<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('webviet.admin.user.index') }}">All users</a>
                    </li>
                    <li>
                        <a href="{{ route('webviet.admin.user.create') }}">New user</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>