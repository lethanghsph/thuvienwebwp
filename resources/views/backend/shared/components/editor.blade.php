<script>
	(function( document, $ ){
		'use strict';
		var baseURL = "{!! url('/') !!}";
		tinymce.init({
			selector:'.wv-tiny-full',
			plugins: [
			   "advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor colorpicker textpattern imagetools code"
			],
			toolbar1: "undo redo | print preview searchreplace | table link unlink hr anchor | image media blockquote code |",
			toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | ltr rtl  visualchars visualblocks | pagebreak nonbreaking insertdatetime |",
			toolbar3: "fontsizeselect formatselect | bold italic underline subscript superscript strikethrough removeformat | forecolor backcolor | charmap emoticons |",
			
			table_class_list: [
				{title: 'Default', value: ''},
				{title: 'Hover', value: 'table table-hover'},
				{title: 'Striped', value: 'table table-striped'},
				{title: 'Border', value: 'table table-bordered'},
				{title: 'Hover Border', value: 'table table-hover table-bordered'},
				{title: 'Striped Border', value: 'table table-striped table-bordered'},
			],

			image_advtab: true,
			toolbar_items_size: 'small',
			menubar: false,

			relative_urls: false,
			file_browser_callback: function(field_name, url, type, win) {
			 	var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
				var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

				var cmsURL = baseURL + '/admin/media?field_name=' + field_name;
				if (type == 'image') {
					cmsURL = cmsURL + "&type=Images";
				} else {
					cmsURL = cmsURL + "&type=Files";
				}

				tinyMCE.activeEditor.windowManager.open({
					file : cmsURL,
					title : 'Filemanager',
					width : x * 0.8,
					height : y * 0.8,
					resizable : "yes",
					close_previous : "no"
				});
			}
		});
	
	})( document, jQuery );
</script>