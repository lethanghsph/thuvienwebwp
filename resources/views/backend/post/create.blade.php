@extends('webviet::backend.layout.main')

@section('content')

    @include('webviet::backend.post.partials.form')

@endsection
@section('unique-js')
    @include('webviet::backend.shared.components.editor')
@endsection