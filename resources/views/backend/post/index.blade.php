@extends('webviet::backend.layout.main')

@section('content')
	<div class="wv-box">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header wv-page-header">
					Posts
					<small><a href="{{Route('webviet.admin.post.create')}}" class="text-primary pl-5" data-tooltip="tooltip" data-placement="right" title="Add new Post"><i class="fa fa-plus-circle"></i></a></small>
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				 <table width="100%" class="table  table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Status</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($posts as $post)
						<tr>
							<td>{{$post->id}}</td>
							<td>{{$post->title}}</td>
							<td>{{$post->publish}}</td>
							<td>{{$post->created_at}}</td>
							<td>
								<a href="<?php echo Route('webviet.admin.post.edit', ['id' => $post->id]) ?>" data-tooltip="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw text-success"></i></a>
								<span data-tooltip="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#modal-delete-{{$post->id}}"><i class="fa fa-trash fa-fw text-success"></i></span>
								@include('webviet::backend.post.partials.modals.delete', ['id' => $post->id])
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>

@endsection
