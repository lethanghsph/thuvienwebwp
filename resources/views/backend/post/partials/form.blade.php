<div class="row">    
    @if(Route::is('webviet.admin.post.create'))
    <form action="{{route('webviet.admin.post.store')}}" method="POST">
        <input type="hidden" name="user_id" value="{{ Auth::id() }}">
        {{ csrf_field() }}
    @else
    <form action="{{route('webviet.admin.post.update', $id)}}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" name="user_id" value="{{$user_id}}">
    @endif
        <div class="col-lg-8">
            <div class="wv-box">
                <h4 class="page-header wv-page-header">
                    @if(Route::is('webviet.admin.post.create'))
                        Create a New Post
                    @else
                        Edit post
                    @endif
                </h4>
                @include('webviet::backend.shared.partials.errors')
                <div class="form-group">
                    <label>Title</label>
                    <input class="form-control" name="title" placeholder="Title" value="{!! $title !!}" />
                </div>
                <div class="form-group">
                    <label>Excerpt</label>
                    <textarea class="form-control" rows="3" name="excerpt" placeholder="Excerpt">{!! $excerpt !!}</textarea>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control wv-tiny-full" rows="20" name="content" placeholder="Content">{!! $content !!}</textarea>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="wv-box">
                <h4 class="page-header wv-page-header">Publish</h4>
                <div class="form-group">
                    <label>Status</label>
                    <label class="radio-inline">
                        <input name="publish" value="visible" {!! 'visible' == $publish ? 'checked' : '' !!} type="radio">Visible
                    </label>
                    <label class="radio-inline">
                        <input name="publish" value="invisible" {!! 'invisible' == $publish ? 'checked' : '' !!} type="radio">Invisible
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">
                    @if(Route::is('webviet.admin.post.create'))
                        Create
                    @else
                        Update
                    @endif
                </button>
                <button type="reset" class="btn btn-warning">Reset</button>
            </div>

            <div class="wv-box">
                <h4 class="page-header wv-page-header">Featured Image</h4>
                <div class="form-group">
                    @include('webviet::backend.shared.fields.image', compact('thumbnail'))
                </div>
            </div>

            <div class="wv-box">
                <h4 class="page-header wv-page-header">Tags</h4>
                <div class="form-group">
                    <select name="tags[]" data-placeholder="Choose tags" class="wv-chosen-select form-control" multiple>
                        @foreach($alltags as $tag)
                            <option value="{!! $tag['id'] !!}" {!! is_selected($tag['id'], $tags) !!} >{!! $tag['title'] !!}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="wv-box">
                <h4 class="page-header wv-page-header">SEO Description</h4>
                <div class="form-group">
                    <textarea class="form-control" name="seo" rows="3" placeholder="SEO description">{!! $seo !!}</textarea>
                </div>
            </div>
        </div>
    <form>
</div>
        