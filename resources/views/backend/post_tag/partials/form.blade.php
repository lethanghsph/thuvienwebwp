<div class="row">
	<div class="col-lg-12">
		<div class="wv-box">
			<h4 class="page-header wv-page-header">
				@if(Route::is('webviet.admin.post_tag.create'))
					Add new Tag
				@else
					Edit Tag
				@endif
			</h4>
			@include('webviet::backend.shared.partials.errors')
			@if(Route::is('webviet.admin.post_tag.create'))
			<form action="{{route('webviet.admin.post_tag.store')}}" method="POST">
			    {{ csrf_field() }}
			@else
			<form action="{{route('webviet.admin.post_tag.update', $id)}}" method="POST">
			    {{ csrf_field() }}
			    {{ method_field('PUT') }}
			@endif
				<div class="form-group">
					<label>Title</label>
					<input class="form-control" data-slug="title" name="title" placeholder="Title" value="{!! $title !!}" />
				</div>
				@if(Route::is('webviet.admin.post_tag.edit'))
				<div class="form-group">
					<label>Slug</label>
					<input class="form-control" data-slug="slug" name="slug" placeholder="Slug" value="{!! $slug !!}" readonly />
				</div>
				@endif
				<div class="form-group">
					<label>Desciptions</label>
					<textarea class="form-control" rows="5" name="description" placeholder="Descriptions">{!! $description !!}</textarea>
				</div>
				<button type="submit" class="btn btn-primary">
					@if(Route::is('webviet.admin.post_tag.create'))
						Add
					@else
						Update
					@endif
				</button>
				<button type="reset" class="btn btn-warning">Reset</button>
			</form>
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
