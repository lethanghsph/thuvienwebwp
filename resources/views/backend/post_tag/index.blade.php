@extends('webviet::backend.layout.main')

@section('content')

	<div class="wv-box">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header wv-page-header">
					Tags
					<small><a href="{{Route('webviet.admin.post_tag.create')}}" class="text-primary pl-5" data-tooltip="tooltip" data-placement="right" title="Add new Tag"><i class="fa fa-plus-circle"></i></a></small>
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<table width="100%" class="table  table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Slug</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tags as $tag)
							<tr>
								<td><?php echo $tag->id ?></td>
								<td><?php echo $tag->title ?></td>
								<td><?php echo $tag->slug ?></td>
								<td>
									<a href="<?php echo Route('webviet.admin.post_tag.edit', ['id' => $tag->id]) ?>" data-tooltip="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw text-success"></i></a>
									<span data-tooltip="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#modal-delete-{{$tag->id}}"><i class="fa fa-trash fa-fw text-success"></i></span>
									@include('webviet::backend.post_tag.partials.modals.delete', ['id' => $tag->id])
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>

@endsection
