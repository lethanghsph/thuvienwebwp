<div class="row">    
    @if(Route::is('webviet.admin.order.create'))
    <form action="{{route('webviet.admin.order.store')}}" method="POST">
        {{ csrf_field() }}
    @else
    <form action="{{route('webviet.admin.order.update', $id)}}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    @endif
        <div class="col-lg-8">
            <div class="wv-box">
                <h4 class="page-header wv-page-header">
                    @if(Route::is('webviet.admin.order.create'))
                        Create a New Order
                    @else
                        Edit order
                    @endif
                </h4>
                @include('webviet::backend.shared.partials.errors')
                <div class="form-group">
                    <label>Product</label>
                    <input type="number" class="form-control" name="product_id" placeholder="Product ID" value="{!! $product_id !!}" />
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" name="name" placeholder="Name" value="{!! $name !!}" />
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="number" class="form-control" name="phone" placeholder="Phone" value="{!! $phone !!}" />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" placeholder="Email" value="{!! $email !!}" />
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input class="form-control" name="address" placeholder="Address" value="{!! $address !!}" />
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea class="form-control" rows="8" name="note" placeholder="Note">{!! $note !!}</textarea>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="wv-box">
                <h4 class="page-header wv-page-header">Publish</h4>
                <div class="form-group">
                    <label>Status</label>
                     <select name="status" class="wv-chosen-select form-control">
                        <option value="waiting" {!! is_selected($status, 'waiting') !!} >Waiting</option>
                        <option value="doing" {!! is_selected($status, 'doing') !!} >Doing</option>
                        <option value="done" {!! is_selected($status, 'done') !!} >Done</option>
                        <option value="cancle" {!! is_selected($status, 'cancle') !!} >Cancle</option>
                        <option value="destroy" {!! is_selected($status, 'destroy') !!} >Destroy</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">
                    @if(Route::is('webviet.admin.order.create'))
                        Create
                    @else
                        Update
                    @endif
                </button>
                <button type="reset" class="btn btn-warning">Reset</button>
            </div>
        </div>
    <form>
</div>
        