@extends('webviet::backend.layout.main')

@section('content')
	<div class="wv-box">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header wv-page-header">
					Orders
					<small><a href="{{Route('webviet.admin.order.create')}}" class="text-primary pl-5" data-tooltip="tooltip" data-placement="right" title="Add new Order"><i class="fa fa-plus-circle"></i></a></small>
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				 <table width="100%" class="table  table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Product ID</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $order)
						<tr>
							<td>{{$order->id}}</td>
							<td>{{$order->name}}</td>
							<td>{{$order->phone}}</td>
							<td>{{$order->email}}</td>
							<td>{{$order->product_id}}</td>
							<td>{{$order->status}}</td>
							<td>
								<a href="<?php echo Route('webviet.admin.order.edit', ['id' => $order->id]) ?>" data-tooltip="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw text-success"></i></a>
								<span data-tooltip="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#modal-delete-{{$order->id}}"><i class="fa fa-trash fa-fw text-success"></i></span>
								@include('webviet::backend.order.partials.modals.delete', ['id' => $order->id])
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>

@endsection
