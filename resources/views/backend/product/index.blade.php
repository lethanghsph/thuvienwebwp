@extends('webviet::backend.layout.main')

@section('content')

	<div class="wv-box">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header wv-page-header">
					Products
					<small><a href="{{Route('webviet.admin.product.create')}}" class="text-primary pl-5" data-tooltip="tooltip" data-placement="right" title="Add new Product"><i class="fa fa-plus-circle"></i></a></small>
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				 <table width="100%" class="table  table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>SKU</th>
							<th>Status</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $product)
						<tr>
							<td>{{$product->id}}</td>
							<td>{{$product->title}}</td>
							<td>{{$product->sku}}</td>
							<td>{{$product->publish}}</td>
							<td>{{$product->created_at}}</td>
							<td>
								<a href="<?php echo Route('webviet.admin.product.edit', ['id' => $product->id]) ?>" data-tooltip="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw text-success"></i></a>
								<span data-tooltip="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#modal-delete-{{$product->id}}"><i class="fa fa-trash fa-fw text-success"></i></span>
								@include('webviet::backend.product.partials.modals.delete', ['id' => $product->id])
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>

@endsection
