@extends('webviet::backend.layout.main')

@section('content')

    <!-- /.row -->
    <div class="row">
        <form action="" method="POST">
            <div class="col-lg-12">
                <div class="wv-box">
                    <h4 class="page-header wv-page-header">Create a New User</h4>
                    <div class="form-group">
                        <label>Username</label>
                        <input class="form-control" name="txtUser" placeholder="Please Enter Username" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="txtPass" placeholder="Please Enter Password" />
                    </div>
                    <div class="form-group">
                        <label>RePassword</label>
                        <input type="password" class="form-control" name="txtRePass" placeholder="Please Enter RePassword" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="txtEmail" placeholder="Please Enter Email" />
                    </div>
                    <div class="form-group">
                        <label>User Level</label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="1" checked="" type="radio">Admin
                        </label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="2" type="radio">Member
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">User Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
            
        <form>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection
