@extends('webviet::backend.layout.main')

@section('content')

    <div class="wv-box">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header wv-page-header">Users</h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                 <table width="100%" class="table  table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeC">
                            <td>Misc</td>
                            <td>PSP browser</td>
                            <td>PSP</td>
                            <td class="center">-</td>
                            <td class="center">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o  fa-fw"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeU">
                            <td>Other browsers</td>
                            <td>All others</td>
                            <td>PSP</td>
                            <td class="center">-</td>
                            <td class="center">
                                <a href="#"><i class="fa fa-pencil fa-fw"></i></a>
                                <a href="#"><i class="fa fa-trash-o  fa-fw"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeC">
                            <td>Misc</td>
                            <td>PSP browser</td>
                            <td>PSP</td>
                            <td class="center">C</td>
                            <td class="center">
                                <a href="#"><i class="fa fa-pencil fa-fw"></i></a>
                                <a href="#"><i class="fa fa-trash-o  fa-fw"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeU">
                            <td>Other browsers</td>
                            <td>All others</td>
                            <td>-</td>
                            <td class="center">U</td>
                            <td class="center">
                                <a href="#"><i class="fa fa-pencil fa-fw"></i></a>
                                <a href="#"><i class="fa fa-trash-o  fa-fw"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

@endsection
