<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('webviet::backend.shared.partials.meta')
        @yield('title')
        @include('webviet::backend.shared.partials.css')
    </head>
    <body>
        @if(!Auth::check())
            @yield('login')
        @else
            <div id="wrapper">
                <!-- Header -->
                @include('webviet::backend.shared.partials.header')
                
                <div id="page-wrapper">
                    <div class="wrap">
                        <!-- Content -->
                        @yield('content')
                    </div>
                    <!-- Footer -->
                    @include('webviet::backend.shared.partials.footer')
                </div>
            </div>
            @include('webviet::backend.shared.partials.js')
            @yield('unique-js')
        @endif
    </body>
</html>
