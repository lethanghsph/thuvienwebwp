@extends('webviet::backend.layout.main')
@section('title')
	<title>Media file manager</title>
@endsection

@section('content')

	<div class="row">
		<div class="col-xs-12">
			<div class="wv-box">
				<iframe class="wv-media" src="{!! url('/admin/media') !!}"></iframe>
			</div>
		</div>
	</div>
@endsection