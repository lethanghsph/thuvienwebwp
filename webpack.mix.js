let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**
 * Backend.
 */
mix.sass('resources/assets/sass/backend/main.scss', 'public/css/backend.min.css')
	.sourceMaps()
	.options({ processCssUrls: false });

mix.js('resources/assets/js/backend/main.js', 'public/js/backend.min.js')
   .sourceMaps();

mix.styles([
    'bower_components/metisMenu/dist/metisMenu.min.css',
    'bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css',
    'bower_components/datatables-responsive/css/dataTables.responsive.css',
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/chosen/chosen.min.css'
], 'public/css/backend.vendor.min.css');

mix.scripts([
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/metisMenu/dist/metisMenu.min.js',
    'bower_components/datatables/media/js/jquery.dataTables.min.js',
    'bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
    'bower_components/datatables-responsive/js/dataTables.responsive.js',
    'bower_components/chosen/chosen.jquery.min.js'
], 'public/js/backend.vendor.min.js');


/**
 * Frontend.
 */
mix.sass('resources/assets/sass/frontend/main.scss', 'public/css/webviet.min.css')
	.sourceMaps()
	.options({ processCssUrls: false });

mix.js('resources/assets/js/frontend/main.js', 'public/js/webviet.min.js')
   .sourceMaps();

mix.styles([
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/slick-carousel/slick/slick.css',
], 'public/css/webviet.vendor.min.css');

mix.scripts([
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/slick-carousel/slick/slick.min.js',
], 'public/js/webviet.vendor.min.js');

/**
 * Copy.
 */
mix.copy('bower_components/jquery/dist/jquery.min.js', 'public/vendor/jquery/jquery.min.js')
	.copy('bower_components/font-awesome/fonts/*', 'public/fonts')
	.copy('bower_components/bootstrap/dist/fonts/*', 'public/fonts')
	.copy('bower_components/chosen/chosen-sprite.png', 'public/css')
	.copy('bower_components/chosen/chosen-sprite@2x.png', 'public/css')
	.copyDirectory('bower_components/tinymce', 'public/vendor/tinymce')
	.copyDirectory('bower_components/bootstrap/dist/fonts/*', 'public/vendor/tinymce');

/**
 * BrowserSynce.
 */
mix.browserSync({
    proxy: '127.0.0.1:8000'
});
