<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'namespace' => 'Frontend',
], function(){
     /* Home page. */
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as'   => 'webviet.frontend.home.index'
    ]);
    /* Products archive - single - taxonomy */
    Route::get('/products', [
        'uses' => 'ProductController@getArchive',
        'as'   => 'webviet.frontend.product.archive'
    ]);
    Route::get('/product/{product}', [
        'uses' => 'ProductController@getSingle',
        'as'   => 'webviet.frontend.product.single'
    ]);
    Route::get('/product-tag/{taxonomy}', [
        'uses' => 'ProductController@getTaxonomy',
        'as'   => 'webviet.frontend.product.taxonomy'
    ]);
    /* Checkout */
    Route::get('/add-to-cart/{product}', [
        'uses' => 'CartController@add',
        'as'   => 'webviet.frontend.cart.add'
    ]);
    Route::get('/checkout', [
        'uses' => 'CheckoutController@getCheckout',
        'as'   => 'webviet.frontend.checkout.index'
    ]);
    Route::post('/checkout', [
        'uses' => 'CheckoutController@postCheckout',
        'as'   => 'webviet.frontend.checkout.store'
    ]);
    Route::get('/thankyou', [
        'uses' => 'CheckoutController@getThankyou',
        'as'   => 'webviet.frontend.thankyou.index'
    ]);
});

/* Backend page routes. */
Route::group([
    // 'middlewareGroups' => RouteHelper::getGeneralMiddlewareGroups(),
    'middleware' => 'auth',
    'namespace'  => 'Backend',
    'prefix'     => 'admin',
], function () {
    /* Admin dashboard page route. */
    Route::get('', function(){
        return view('webviet::backend.layout.main');
    })->name('webviet.admin');

    /* Media route */
    Route::get('media/show',[
        'uses' => 'MediaController@index',
        'as'   => 'webviet.admin.media.index',
    ]);

    /* Post page routes. */
    Route::resource('/post', 'PostController', [
        'except' => 'show',
        'names'  => [
            'index'   => 'webviet.admin.post.index',
            'create'  => 'webviet.admin.post.create',
            'store'   => 'webviet.admin.post.store',
            'edit'    => 'webviet.admin.post.edit',
            'update'  => 'webviet.admin.post.update',
            'destroy' => 'webviet.admin.post.destroy',
        ],
    ]);

    /* Post tags page routes. */
    Route::resource('/post-tag', 'PostTagController', [
        'except' => ['show'],
        'names' => [
            'index'   => 'webviet.admin.post_tag.index',
            'create'  => 'webviet.admin.post_tag.create',
            'store'   => 'webviet.admin.post_tag.store',
            'edit'    => 'webviet.admin.post_tag.edit',
            'update'  => 'webviet.admin.post_tag.update',
            'destroy' => 'webviet.admin.post_tag.destroy',
        ],
    ]);

    /* Product page routes. */
    Route::resource('/product', 'ProductController', [
        'except' => 'show',
        'names'  => [
            'index'   => 'webviet.admin.product.index',
            'create'  => 'webviet.admin.product.create',
            'store'   => 'webviet.admin.product.store',
            'edit'    => 'webviet.admin.product.edit',
            'update'  => 'webviet.admin.product.update',
            'destroy' => 'webviet.admin.product.destroy',
        ],
    ]);

    /* Product tags page routes. */
    Route::resource('/product-tag', 'ProductTagController', [
        'except' => ['show'],
        'names' => [
            'index'   => 'webviet.admin.product_tag.index',
            'create'  => 'webviet.admin.product_tag.create',
            'store'   => 'webviet.admin.product_tag.store',
            'edit'    => 'webviet.admin.product_tag.edit',
            'update'  => 'webviet.admin.product_tag.update',
            'destroy' => 'webviet.admin.product_tag.destroy',
        ],
    ]);

    /* Product tags page routes. */
    Route::resource('/order', 'OrderController', [
        'except' => ['show'],
        'names' => [
            'index'   => 'webviet.admin.order.index',
            'create'  => 'webviet.admin.order.create',
            'store'   => 'webviet.admin.order.store',
            'edit'    => 'webviet.admin.order.edit',
            'update'  => 'webviet.admin.order.update',
            'destroy' => 'webviet.admin.order.destroy',
        ],
    ]);

    /* User page routes. */
    Route::resource('/user', 'UserController', [
        'except' => 'show',
        'names'  => [
            'index'   => 'webviet.admin.user.index',
            'create'  => 'webviet.admin.user.create',
            'store'   => 'webviet.admin.user.store',
            'edit'    => 'webviet.admin.user.edit',
            'update'  => 'webviet.admin.user.update',
            'destroy' => 'webviet.admin.user.destroy',
        ],
    ]);
});

/* Authentication */
Route::group([
    'prefix' => 'admin',
], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    // Auth::routes();
});




Route::get('/home', 'HomeController@index')->name('home');
