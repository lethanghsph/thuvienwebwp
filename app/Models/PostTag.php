<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PostTag extends Model
{
    use Sluggable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wv_post_tags';

    /**
     * Disable timestamps.
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        // 'alias',
        'description',
    ];

    /**
     * Searchable items.
     *
     * @var array
     */
    public $searchable = [
        'title',
        'description',
    ];

	/**
	 * Relationship to Post model.
	 */
    public function post()
    {
    	return $this->belongsToMany('App\Models\Post', 'wv_post_tag_relationships', 'tag_id', 'post_id');
    }

    /**
     * Relationship to Order model.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'product_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
