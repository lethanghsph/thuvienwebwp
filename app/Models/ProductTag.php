<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductTag extends Model
{
    use Sluggable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wv_product_tags';

    /**
     * Disable timestamps.
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        // 'alias',
        'description',
    ];

    /**
     * Searchable items.
     *
     * @var array
     */
    public $searchable = [
        'title',
        'description',
    ];

	/**
	 * Relationship to Product model.
	 */
    public function product()
    {
    	return $this->belongsToMany('App\Models\Product', 'wv_product_tag_relationships', 'tag_id', 'product_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
