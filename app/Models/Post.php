<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use Sluggable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wv_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'thumbnail',
        'seo',
        'publish',
        'user_id',
    ];

    /**
     * Searchable items.
     *
     * @var array
     */
    public $searchable = [
        'title',
        'excerpt',
        'content',
        'seo',
    ];

	/**
	 * Relationship to User model.
	 */
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    /**
	 * Relationship to PostTag model.
	 */
    public function tag()
    {
        return $this->belongsToMany('App\Models\PostTag', 'wv_post_tag_relationships', 'post_id', 'tag_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
