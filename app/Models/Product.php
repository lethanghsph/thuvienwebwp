<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Sluggable;

    /**
     * The database table use by the model.
     *
     * @var string
     */
    protected $table = 'wv_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var [array]
     */
    protected $fillable = [
    	'title',
    	'slug',
    	'sku',
    	'demo',
    	'content',
    	'seo',
    	'thumbnail',
    	'publish',
    	'user_id',
    ];

    /**
     * Searchable items.
     *
     * @var array
     */
    public $searchable = [
        'title',
        'sku',
        'demo',
        'excerpt',
        'content',
        'seo',
    ];

    /**
     * Define relationship to User model.
     */
    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Relationship to ProductTag model.
     */
    public function tag()
    {
        return $this->belongsToMany('App\Models\ProductTag', 'wv_product_tag_relationships', 'product_id', 'tag_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['title', 'sku'],
            	'separator' => '-'
            ]
        ];
    }
}
