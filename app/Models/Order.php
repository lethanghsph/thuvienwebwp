<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wv_orders';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'note',
        'product_id',
        'status',
    ];

    /**
     * Relationship...
     */
    public function product()
    {
    	return $this->hasOne('App\Product', 'product_id');
    }
}
