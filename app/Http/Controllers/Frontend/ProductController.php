<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductTag;

class ProductController extends Controller
{
	/**
	 * Products page layout.
	 */
    public function getArchive()
    {
    	$products = Product::paginate(8);
    	return view('webviet::frontend.archive-product', compact(['products']));
    }

	/**
	 * Single page layout.
	 */
    public function getSingle(Request $request, Product $product)
    {
        // Set tags.
    	$tags = $product->tag()->get(['id', 'title']);
        $tag_html = [];
        foreach ($tags as $tag) {
            $tag_html[] = '<a href="'. route('webviet.frontend.product.taxonomy', $tag['id']) .'">'. $tag['title'] .'</a>';
        }
        $tag_html = implode($tag_html, ', ');
        $product->tags = $tag_html;
    	$product = $product->toArray();
    	return view('webviet::frontend.single-product', ['product' => $product]);
    }

    /**
     * Taxonomy page layout.
     */
    public function getTaxonomy(ProductTag $taxonomy)
    {
        $products = $taxonomy->product()->paginate(8);
        return view('webviet::frontend.taxonomy-product', ['products' => $products, 'taxonomy' => $taxonomy->title ]);
    }

}
