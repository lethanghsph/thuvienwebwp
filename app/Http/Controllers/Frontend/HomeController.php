<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class HomeController extends Controller
{
    /**
	 * Products page layout.
	 */
    public function index()
    {
    	$products = Product::paginate(8);
    	return view('webviet::frontend.home', compact(['products']));
    }
}
