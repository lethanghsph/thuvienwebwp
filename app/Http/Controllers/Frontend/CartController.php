<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Session;

class CartController extends Controller
{
    public function add(Request $request,Product $product)
    {
    	$request->session()->put('cart', $product->toArray());
    	return redirect()->route('webviet.frontend.checkout.index');
    }
}
