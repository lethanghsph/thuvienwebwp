<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use App\Models\Order;
use App\Events\CheckoutSentEvent;

class CheckoutController extends Controller
{
	/**
	 * Show the form for creating a new checkout.
	 *
	 * @param  Request $request [description]
	 */
    public function getCheckout()
    {
    	return view('webviet::frontend.checkout');
    }

    /**
     * Checkout.
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCheckout(CheckoutRequest $request)
    {
    	$order = Order::create($request->fillData());
        // CheckoutSentEvent event.
        event(new CheckoutSentEvent($order));
        return redirect()->route('webviet.frontend.thankyou.index');
    }

    public function getThankyou()
    {
        return view('webviet::frontend.thankyou');
    }
}
