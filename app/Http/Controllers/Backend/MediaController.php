<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    /**
     * Display responsive file manager.
     */
    public function index()
    {
    	return view('webviet::backend.media.index');
    }
}
