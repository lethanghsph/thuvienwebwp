<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductTagCreateRequest;
use App\Http\Requests\ProductTagUpdateRequest;

class ProductTagController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = DB::table('wv_product_tags')->get();
        return view('webviet::backend.product_tag.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = app('App\Http\Filters\ProductTagFilter')->handle();
        return view('webviet::backend.product_tag.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\TagCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductTagCreateRequest $request)
    {
        $tag = ProductTag::create($request->fillData());
        
        return redirect()->route('webviet.admin.product_tag.edit', ['id' => $tag->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $productTag
     * @return \Illuminate\Http\Response
     */
    public function show(ProductTag $productTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductTag  $productTag
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductTag $productTag)
    {
        $data = app('App\Http\Filters\ProductTagFilter')->handle($productTag);
        return view('webviet::backend.product_tag.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductTag  $productTag
     * @return \Illuminate\Http\Response
     */
    public function update(ProductTagUpdateRequest $request, ProductTag $productTag)
    {
        $productTag->update($request->fillData());
        return redirect()->route('webviet.admin.product_tag.edit', ['id' => $productTag->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductTag  $productTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductTag $productTag)
    {
        $productTag->delete();
        return redirect()->route('webviet.admin.product_tag.index');
    }
}
