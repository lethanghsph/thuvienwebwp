<?php

namespace App\Http\Controllers\Backend;

use App\Models\PostTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostTagCreateRequest;
use App\Http\Requests\PostTagUpdateRequest;

class PostTagController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = DB::table('wv_post_tags')->get();
        return view('webviet::backend.post_tag.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = app('App\Http\Filters\PostTagfilter')->handle();
        return view('webviet::backend.post_tag.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PostTagCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostTagCreateRequest $request)
    {
        $tag = PostTag::create($request->fillData());
        return redirect()->route('webviet.admin.post_tag.edit', ['id' => $tag->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(PostTag $postTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostTag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(PostTag $postTag)
    {
        $data = app('App\Http\Filters\PostTagfilter')->handle($postTag);
        return view('webviet::backend.post_tag.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostTag  $postTag
     * @return \Illuminate\Http\Response
     */
    public function update(PostTagUpdateRequest $request, PostTag $postTag)
    {
        $postTag->update($request->fillData());
        return redirect()->route('webviet.admin.post_tag.edit', ['id' => $postTag->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostTag  $postTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostTag $postTag)
    {
        $postTag->delete();
        return redirect()->route('webviet.admin.post_tag.index');
    }
}
