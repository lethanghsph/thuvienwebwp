<?php
namespace App\Http\Filters;

use App\Models\Order;

/**
 * Filter data for create and edit blade template.
 */
Class OrderFilter
{
	/**
     * The value that are mass assignable.
     */
	protected $view_fillable = [
		'name'       => '',
		'phone'      => '',
		'email'      => '',
		'address'    => '',
		'note'       => '',
		'product_id' => '',
		'status'     => 'waiting',
    ];

	/**
	 * Return key-value form.
	 *
	 * @return [type] [description]
	 */
	public function handle($model = null)
	{
		$fields = $this->view_fillable;
		if ($model) {
			$fields = $this->fieldsFromModel($model, $fields);
		}
		// Merge session value.
		foreach ($fields as $key => $value) {
			$fields[$key] = old($key, $value);
		}
		return $fields;

	}

	/**
	 * Value field from model.
	 *
	 * @param  Order   $model  [model instance]
	 * @param  array  $fields [list field].
	 */
	protected function fieldsFromModel(Order $post, $fields )
	{
		$fields_value['id'] = $post->id;
		foreach ($fields as $key => $value) {
			$fields_value[$key] = null !== ($post->$key) ? $post->$key : '';
		}
		return $fields_value;
	}
}