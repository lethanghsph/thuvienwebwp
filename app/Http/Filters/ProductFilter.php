<?php
namespace App\Http\Filters;

use App\Models\Product;
use App\Models\ProductTag;

/**
 * Filter data for create and edit blade template.
 */
Class ProductFilter
{
	/**
     * The value that are mass assignable.
     */
	protected $view_fillable = [
		'title'     => '',
		'slug'      => '',
		'sku'       => '',
		'demo'      => '',
		'content'   => '',
		'seo'       => '',
		'thumbnail' => '',
		'publish'   => 'invisible',
		'user_id'   => '',
		'tags'      => '',
    ];

	/**
	 * Return key-value form.
	 *
	 * @return [type] [description]
	 */
	public function handle($model = null)
	{
		$fields = $this->view_fillable;
		if ($model) {
			$fields = $this->fieldsFromModel($model, $fields);
		}
		// Merge session value.
		foreach ($fields as $key => $value) {
			$fields[$key] = old($key, $value);
		}
		// All tags.
		$fields['alltags'] = ProductTag::all(['id', 'title'])->toArray();
		return $fields;

	}

	/**
	 * Value field from model.
	 *
	 * @param  Post   $model  [model instance]
	 * @param  array  $fields [list field].
	 */
	protected function fieldsFromModel(Product $post, $fields )
	{
		$fields_value['id'] = $post->id;
		foreach ($fields as $key => $value) {
			$fields_value[$key] = null !== ($post->$key) ? $post->$key : '';
		}
		$fields_value['tags'] = $post->tag()->pluck('id')->all();
		return $fields_value;
	}
}