<?php
namespace App\Http\Filters;

use App\Models\PostTag;

/**
 * Filter data for create and edit blade template.
 */
Class PostTagFilter
{
	/**
     * The value that are mass assignable.
     */
	protected $view_fillable = [
		'title'       => '',
        'slug'        => '',
        'description' => '',
    ];

	/**
	 * Return key-value form.
	 *
	 * @return [type] [description]
	 */
	public function handle($model = null)
	{
		$fields = $this->view_fillable;
		if ($model) {
			$fields = $this->fieldsFromModel($model, $fields);
		}
		// Merge session value.
		foreach ($fields as $key => $value) {
			$fields[$key] = old($key, $value);
		}
		return $fields;
	}

	/**
	 * Value field from model.
	 *
	 * @param  Post   $model  [model instance]
	 * @param  array  $fields [list field].
	 */
	protected function fieldsFromModel(PostTag $post, $fields )
	{
		$fields_value['id'] = $post->id;
		foreach ($fields as $key => $value) {
			$fields_value[$key] = null !== ($post->$key) ? $post->$key : '';
		}
		return $fields_value;
	}
}