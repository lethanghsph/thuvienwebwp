<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'required|string|max:255',
            'user_id' => 'integer',
            'sku'     => 'required|unique:wv_products,sku,' . $this->product->id,
            'publish' => [
                    'required',
                     Rule::in(['visible', 'invisible']),
                ]
        ];
    }

    /**
     * Return the field and value to create a new post.
     *
     * @return [array]
     */
    public function fillData()
    {
        return [
            'title'     => $this->title,
            'sku'       => isset($this->sku) ? $this->sku : '',
            'demo'      => isset($this->demo) ? $this->demo : '',
            'content'   => null !== $this->get('content') ? $this->get('content') : '',
            'thumbnail' => null !== $this->get('thumbnail') ? $this->get('thumbnail') : '',
            'seo'       => isset($this->seo) ? $this->seo : '',
            'publish'   => $this->publish,
            'user_id'   => $this->user_id,
        ];
    }
}
