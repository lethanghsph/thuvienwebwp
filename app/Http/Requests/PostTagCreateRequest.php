<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostTagCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            // 'slug'  => 'required|unique:wv_tags,slug|string',
        ];
    }

    /**
     * Return the fields and values to create a new resource.
     */
    public function fillData()
    {
        return [
            'title'       => $this->title,
            // 'slug'        => $this->title,
            'description' => isset($this->description) ? $this->description : ''
        ];
    }
}
