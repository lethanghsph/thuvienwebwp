<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string|max:255',
            'phone'      => 'required|regex:/[0-9]{9,11}/',
            'email'      => 'required|email',
            'product_id' => 'required|integer',
            'status' => [
                    'required',
                     Rule::in(['waiting', 'doing', 'done', 'cancle', 'destroy']),
                ]
        ];
    }

    /**
     * Return the field and value to create a new post.
     *
     * @return [array]
     */
    public function fillData()
    {
        return [
            'name'       => $this->name,
            'phone'      => $this->phone,
            'email'      => $this->email,
            'address'    => isset($this->address) ? $this->address : '',
            'note'       => null !== $this->get('note') ? $this->get('note') : '',
            'product_id' => $this->product_id,
            'status'     => null !== $this->get('status') ? $this->get('status') : 'waiting',
        ];
    }
}
