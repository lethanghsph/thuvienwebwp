<?php

namespace App\Listeners;

use App\Events\CheckoutSentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\OrderNew;
use Mail;

class SendEmailToAdminListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';

    /**
     * Handle the event.
     *
     * @param  CheckoutSentEvent  $event
     * @return void
     */
    public function handle(CheckoutSentEvent $event)
    {
        $email = new OrderNew($event);
        Mail::to(config('mail.from.address'))->send($email);
    }
}
