<?php

/**
 * Declare extra functions.
 */

/**
 * List all categories in table.
 */
function wv_list_cates($categories, $parent_id = 0, $pre = "")
{
	foreach ($categories as $post_cate) {
		$id = isset( $post_cate['id'] ) ? $post_cate['id'] : null;
		if (is_null($id)) {continue;}

		if ( $post_cate['parent_id'] == $parent_id ) { ?>
			<tr>
	            <td><?php echo $post_cate['id'] ?></td>
	            <td><?php echo $pre . $post_cate['title'] ?></td>
	            <td><?php echo $post_cate['alias'] ?></td>
	            <td><?php echo $post_cate['parent_id'] ?></td>
	            <td>
	                <a href="<?php echo Route('webviet.admin.post_cate.edit', ['id' => $post_cate['id']]) ?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil fa-fw text-success"></i></a>
	            </td>
	        </tr>
	        <?php
			wv_list_cates( $categories, $id, $pre . "--");
		}
	}
}

/**
 * Check if option is selected.
 *
 * @param  [string]       $key   [option value]
 * @param  [string|array] $value [value]
 */
function is_selected($key, $value) {
	$selected = '';
	if ( is_string($value) ) {
		if ($key === $value) {
			$selected = 'selected';
		}
	} elseif (is_array($value)) {
		if ( in_array($key, $value) ) {
			$selected = 'selected';
		}
	}
	return $selected;
}

