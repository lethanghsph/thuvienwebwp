<?php

namespace App\Helpers;

class FormHelper
{
	/**
	 * Generate hierachy categories select template.
	 * 
	 * @param  [array]   $cats      [all categories].
	 * @param  [integer] $parent_id [parent category id].
	 * @param  string    $pre       [name of category].
	 * @param  integer   $select    [selected id].
	 * @return [type]               [template.]
	 */
	public static function catesSelect( $cats, $parent_id = 0, $pre = "", $select = 0 )
	{
		foreach ($cats as $cat) {
			// Set title and id.
			$id = isset( $cat['id'] ) ? $cat['id'] : null;
			if (is_null($id)) {continue;}
			$title = isset($cat['title']) ? $cat['title'] : '';

			if ( $cat['parent_id'] == $parent_id ) {
				if ( 0 != $select && $id == $select ) {
					echo "<option value=$id selected='selected'>$pre $title</option>";
				} else {
					echo "<option value=$id >$pre $title</option>";
				}
				static::catesSelect( $cats, $id, $pre . "--");
			}
		}
	}
	/**
	 * Validate alias.
	 */
	public function alias()
	{
		//
	}
}