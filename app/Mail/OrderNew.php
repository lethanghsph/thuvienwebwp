<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CheckoutSentEvent;
use App\Models\Product;

class OrderNew extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Order.
     *
     * @var [type]
     */
    public $order;

    /**
     * Create a new message instance.
     * 
     * @return void
     */
    public function __construct(CheckoutSentEvent $order)
    {
        $this->order = $order->order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $product = Product::find($order->product_id)->first();
        return $this->view('emails.order.new')->with(['order' => $order, 'product' => $product ]);
    }
}
