<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Change default length string database.
        Schema::defaultStringLength(191);
        // Cart view.
        view()->composer('webviet::frontend.checkout', function($view){
            $product = [];
            if (session('cart')) {
                $product = session('cart');
            }
            $view->with(compact('product'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
