<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use App\Helpers\FormHelper;

class WebvietServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Bootstrap namespace for all view.
        $this->handleViews();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $loader = AliasLoader::getInstance();

        // Register facades...
        $loader->alias('WebvietForms', FormHelper::class);
    }

    private function handleViews()
    {
        // Load view from ...
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'webviet');
    }

}
