<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ProductTag::class, function (Faker $faker) {
    $id = $faker->unique()->numberBetween($min = 1, $max = 99);
    return [
        'id'    => $id,
        'title' => 'Tag ' . $id,
        'slug' => 'tag-' . $id,
        'description' => $faker->text($maxNbChars = 100)
    ];
});
