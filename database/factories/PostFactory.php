<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $id = $faker->unique()->numberBetween($min = 1, $max = 99);
    return [
        'id' => $id,
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'slug' => 'webviet-slug-' . $id,
        'excerpt' => $faker->paragraphs($nb = 3, $asText = true),
        'content' => $faker->paragraphs($nb = 10, $asText = true),
        'thumbnail' => 'http://127.0.0.1:8000/images/thumbnail.jpg',
        'seo' => $faker->words($nb = 3, $asText = true),
        'publish' => 'visible',
        'user_id' => '1',
    ];
});
