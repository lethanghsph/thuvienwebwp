<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PostTagsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(ProductTagsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }
}
