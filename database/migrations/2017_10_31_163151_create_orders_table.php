<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wv_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('phone')->unsigned();
            $table->string('email');
            $table->string('address');
            $table->text('note');
            $table->string('status');
            $table->integer('product_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wv_orders');
    }
}
